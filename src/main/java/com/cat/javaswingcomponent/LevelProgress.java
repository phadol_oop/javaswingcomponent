/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

/**
 *
 * @author Black Dragon
 */
public class LevelProgress extends JFrame {

    JProgressBar jb;
    int level = 1;
    JLabel label;
    JButton b1;
    JButton b2;
    JButton b3;
    JButton b4;

    public LevelProgress() {
        super("Progress");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(null);

        label = new JLabel("Current Level: " + level);
        label.setSize(150, 30);
        label.setLocation(20, 20);
        label.setOpaque(true);
        this.add(label);

        jb = new JProgressBar(0, 84);
        jb.setBounds(20, 60, 200, 20);
        jb.setValue(0);
        jb.setStringPainted(true);
        this.add(jb);

        b1 = new JButton("10 XP");
        b1.setBounds(20, 90, 100, 20);
        this.add(b1);

        b2 = new JButton("100 XP");
        b2.setBounds(130, 90, 100, 20);
        this.add(b2);

        b3 = new JButton("1000 XP");
        b3.setBounds(20, 120, 100, 20);
        this.add(b3);

        b4 = new JButton("10000 XP");
        b4.setBounds(130, 120, 100, 20);
        this.add(b4);

        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addXp(10);
                jb.setMaximum((int)(84 * (Math.pow(level, 2))));
            }
        });

        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addXp(100);
                jb.setMaximum((int)(84 * (Math.pow(level, 2))));
            }
        });

        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addXp(1000);
                jb.setMaximum((int)(84 * (Math.pow(level, 2))));
            }
        });

        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addXp(10000);
                jb.setMaximum((int)(84 * (Math.pow(level, 2))));
            }
        });

    }

    public void addXp(int n) {
        if (n+jb.getValue() >= (int)(84 * (Math.pow(level, 2)))) {
            n += jb.getValue();
            n -= jb.getMaximum();
            level++;
            jb.setValue(0);
            addXp(n);
        } else {
            n += jb.getValue();
            jb.setValue(n);
            n = 0;
            label.setText("Current Level: " + level);
            System.out.println("Current xp :" + jb.getValue() + " / " + jb.getMaximum());
        }
    }

    public static void main(String[] args) {
        LevelProgress frame = new LevelProgress();
        frame.setVisible(true);
    }
}
