/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Black Dragon
 */
public class RegisterFrame extends JFrame {

    JButton btnBack;
    JLabel lblID;
    JTextField idField;
    JLabel lblPassword;
    JPasswordField passField;
    JLabel lblConfirmPassword;
    JPasswordField confirmpassField;
    JButton btnRegister;

    IDsession[] session = new IDsession[20];
    private int used = 0;

    public RegisterFrame() {
        super("Register");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(null);

        lblID = new JLabel("ID:", JLabel.TRAILING);
        lblID.setSize(100, 20);
        lblID.setLocation(20, 20);
        lblID.setOpaque(true);
        this.add(lblID);

        idField = new JTextField();
        idField.setSize(75, 20);
        idField.setLocation(125, 20);
        this.add(idField);

        lblPassword = new JLabel("Password:", JLabel.TRAILING);
        lblPassword.setSize(100, 20);
        lblPassword.setLocation(20, 50);
        lblPassword.setOpaque(true);
        this.add(lblPassword);

        passField = new JPasswordField();
        passField.setSize(75, 20);
        passField.setLocation(125, 50);
        this.add(passField);

        lblConfirmPassword = new JLabel("Confirm Password:", JLabel.TRAILING);
        lblConfirmPassword.setSize(120, 20);
        lblConfirmPassword.setLocation(0, 80);
        lblConfirmPassword.setOpaque(true);
        this.add(lblConfirmPassword);

        confirmpassField = new JPasswordField();
        confirmpassField.setSize(75, 20);
        confirmpassField.setLocation(125, 80);
        this.add(confirmpassField);

        btnBack = new JButton("Back");
        btnBack.setBounds(60, 125, 70, 20);
        this.add(btnBack);

        btnBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                dispose();
                LoginFrame frame = new LoginFrame();
                frame.setVisible(true);
            }
        });

        btnRegister = new JButton("Register");
        btnRegister.setSize(100, 20);
        btnRegister.setLocation(135, 125);
        this.add(btnRegister);

        btnRegister.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    LoginFrame logFrame = new LoginFrame();
                    String pass = passField.getText();
                    String comfpass = confirmpassField.getText();
                    if (!(pass.equals(comfpass))) {
                        JOptionPane.showMessageDialog(RegisterFrame.this, "Error : Password did not match.",
                                "Error", JOptionPane.ERROR_MESSAGE);
                        passField.setText("");
                        confirmpassField.setText("");
                    } else {
                        setVisible(false);
                        for (int i = 0; i < used; i++) {
                            logFrame.createNewID(session[i].getUserID(), session[i].getUserPassword());
                        }
                        logFrame.createNewID(idField.getText(), passField.getText());
                        logFrame.setVisible(true);
                        dispose();
                    }
                } catch (Exception ex) {

                }
            }
        });
    }

    public void createNewID(String id, String pass) {
        session[used] = new IDsession(id, pass);
        used++;
    }
}
