/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javaswingcomponent;

/**
 *
 * @author Black Dragon
 */
public class IDsession {

    private String userID;
    private String userPassword;

    public IDsession(String userID, String userPassword) {
        this.userID = userID;
        this.userPassword = userPassword;
    }

    public String getUserID() {
        return userID;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public boolean login(String id, String password) {
        if (id.equals(userID) && password.equals(userPassword)) {
            return true;
        }
        return false;
    }
}
