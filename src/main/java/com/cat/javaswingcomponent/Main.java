/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;

/**
 *
 * @author Black Dragon
 */
public class Main extends JFrame {

    JLabel label;

    public Main() {
        super("Main");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        label = new JLabel("Select program.");
        label.setSize(100, 30);
        label.setLocation(20, 20);
        label.setOpaque(true);

        JButton b = new JButton("Show");
        b.setBounds(150, 75, 80, 30);

        final DefaultListModel<String> l1 = new DefaultListModel<>();
        l1.addElement("Login");
        l1.addElement("Status");
        l1.addElement("Quiz");
        l1.addElement("LevelBar");

        final JList<String> list1 = new JList<>(l1);
        list1.setBounds(30, 50, 75, 75);

        this.add(label);
        this.add(list1);
        this.add(b);

        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (list1.getSelectedIndex() != -1) {
                    switch (list1.getSelectedIndex()) {
                        case 0:
                            LoginFrame loginFrame = new LoginFrame();
                            loginFrame.setVisible(true);
                            break;
                        case 1:
                            JDPaneDemo status = new JDPaneDemo();
                            status.setVisible(true);
                            break;
                        case 2:
                            Quiz quiz = new Quiz();
                            quiz.setVisible(true);
                            break;
                        case 3:
                            LevelProgress bar = new LevelProgress();
                            bar.setVisible(true);
                            break;
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        Main frame = new Main();
        frame.setVisible(true);
    }
}
