/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Black Dragon
 */
public class LoginFrame extends JFrame {

    JLabel lblID;
    JTextField idField;
    JLabel lblPassword;
    JPasswordField passField;
    JButton btnLogin;
    JButton btnRegister;

    IDsession[] session = new IDsession[20];
    private int used = 0;

    public LoginFrame() {
        super("Login");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(null);

        lblID = new JLabel("ID:", JLabel.TRAILING);
        lblID.setSize(100, 20);
        lblID.setLocation(20, 30);
        lblID.setOpaque(true);
        this.add(lblID);

        idField = new JTextField();
        idField.setSize(75, 20);
        idField.setLocation(125, 30);
        this.add(idField);

        lblPassword = new JLabel("Password:", JLabel.TRAILING);
        lblPassword.setSize(100, 20);
        lblPassword.setLocation(20, 60);
        lblPassword.setOpaque(true);
        this.add(lblPassword);

        passField = new JPasswordField();
        passField.setSize(75, 20);
        passField.setLocation(125, 60);
        this.add(passField);

        btnLogin = new JButton("Login");
        btnLogin.setSize(100, 20);
        btnLogin.setLocation(45, 100);
        this.add(btnLogin);

        btnLogin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    char c = 'a';//active
                    for (int i = 0; i < used; i++) {
                        if (session[i].login(idField.getText(), passField.getText())) {
                            JOptionPane.showMessageDialog(LoginFrame.this, "access granted.",
                                    "OK", JOptionPane.OK_OPTION);
                            c = 'i'; //inactive
                            break;
                        }
                    }
                    if (c == 'a') {
                        JOptionPane.showMessageDialog(LoginFrame.this, "Error : Username or password are not match.",
                                "Error", JOptionPane.ERROR_MESSAGE);
                        passField.setText("");
                    }
                } catch (Exception ex) {
                    
                }
            }
        });

        btnRegister = new JButton("Register");
        btnRegister.setSize(100, 20);
        btnRegister.setLocation(150, 100);
        this.add(btnRegister);

        btnRegister.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                RegisterFrame regframe = new RegisterFrame();
                regframe.setVisible(true);
                for(int i = 0; i < used; i++){
                    regframe.createNewID(session[i].getUserID(), session[i].getUserPassword());
                }
                dispose();
            }
        });
    }
    
    public void createNewID(String id, String pass) {
        session[used] = new IDsession(id, pass);
        used++;
    }
}
