/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 *
 * @author Black Dragon
 */
public class Quiz extends JFrame {

    JLabel lblText1;
    JLabel lblText2;
    JLabel lblText3;
    JButton btnConfirm;

    public Quiz() {
        super("Quiz");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        this.setLayout(null);

        lblText1 = new JLabel("Coding skill:");
        lblText1.setSize(100, 30);
        lblText1.setLocation(20, 10);
        lblText1.setOpaque(true);
        this.add(lblText1);

        JCheckBox check1 = new JCheckBox("C++");
        check1.setBounds(20, 40, 50, 20);
        this.add(check1);

        JCheckBox check2 = new JCheckBox("Java");
        check2.setBounds(70, 40, 52, 20);
        this.add(check2);

        JCheckBox check3 = new JCheckBox("HTML");
        check3.setBounds(120, 40, 70, 20);
        this.add(check3);

        lblText2 = new JLabel("Education:");
        lblText2.setSize(100, 20);
        lblText2.setLocation(20, 70);
        lblText2.setOpaque(true);
        this.add(lblText2);

        JRadioButton r1 = new JRadioButton("Primary education");
        JRadioButton r2 = new JRadioButton("Secondary education");
        JRadioButton r3 = new JRadioButton("Tertiary education");
        r1.setBounds(20, 100, 150, 20);
        r2.setBounds(20, 120, 150, 20);
        r3.setBounds(20, 140, 150, 20);
        ButtonGroup bg = new ButtonGroup();
        bg.add(r1);
        bg.add(r2);
        bg.add(r3);
        this.add(r1);
        this.add(r2);
        this.add(r3);

        lblText3 = new JLabel("Country:");
        lblText3.setSize(100, 20);
        lblText3.setLocation(20, 170);
        lblText3.setOpaque(true);
        this.add(lblText3);

        String country[] = {"India", "Aus", "U.S.A.", "England", "Newzealand", "Thailand"};
        JComboBox cb = new JComboBox(country);
        cb.setBounds(20, 200, 90, 20);
        this.add(cb);

        btnConfirm = new JButton("Confirm");
        btnConfirm.setSize(80, 20);
        btnConfirm.setLocation(160, 200);
        this.add(btnConfirm);

        btnConfirm.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(Quiz.this,"Confirmed.");  
            }
        });
    }

    public static void main(String[] args) {
        Quiz frame = new Quiz();
        frame.setVisible(true);
    }
}
