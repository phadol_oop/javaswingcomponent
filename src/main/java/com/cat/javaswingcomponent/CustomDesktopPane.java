/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javaswingcomponent;

import java.awt.Container;
import javax.swing.JDesktopPane;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

/**
 *
 * @author Black Dragon
 */
class CustomDesktopPane extends JDesktopPane {

    public void display(CustomDesktopPane dp) {

        JInternalFrame jframe1 = new JInternalFrame("HP: ", true, true, true, true);
        jframe1.setBounds(30, 30, 250, 85);
        Container c1 = jframe1.getContentPane();
        c1.add(new JLabel("100 / 100"));
        dp.add(jframe1);
        jframe1.setVisible(true);
        
        JInternalFrame jframe2 = new JInternalFrame("MP: ", true, true, true, true);
        jframe2.setBounds(30, 115, 250, 85);
        Container c2 = jframe2.getContentPane();
        c2.add(new JLabel("20 / 50"));
        dp.add(jframe2);
        jframe2.setVisible(true);
        
        JInternalFrame jframe3 = new JInternalFrame("Skill: ", true, true, true, true);
        jframe3.setBounds(30, 200, 250, 85);
        Container c3 = jframe3.getContentPane();
        c3.add(new JLabel("Slash / Run"));
        dp.add(jframe3);
        jframe3.setVisible(true);
    }
}
